# koa-Stormpath

[![NPM Version](https://img.shields.io/npm/v/koa-stormpath.svg?style=flat)](https://npmjs.org/package/koa-stormpath)
[![NPM Downloads](http://img.shields.io/npm/dm/koa-stormpath.svg?style=flat)](https://npmjs.org/package/koa-stormpath)

koa-Stormpath for [koa.js](http://koajs.com/) is based off [express-stormpath](https://github.com/stormpath/express-stormpath)

*The only difference between this package and express-stormpath is the use of koa to handle the api requests.

*Changelog:
* 1.0.1 - inserted a settings collection to mimic expressjs

## License

Apache 2.0, see [LICENSE](LICENSE).
